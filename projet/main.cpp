#include "geometry.h"
#include "io.h"
#include "hermeline.h"
#include <iostream>
#include <random>

int main() {
    std::cout << CANVAS_HELP << std::endl;

    canvas_clear(Color(20, 20, 20));

    std::vector<affine::Point> pts;

    affine::Triangulation triangulation;
    affine::Triangulation old_triangulation;

    CANVAS_KEY_PRESSED_CALLBACK = [&](Key k, const affine::Point& pt)
    {
        if (k == Key::A) {
            pts.push_back(pt);

            canvas_clear();
            old_triangulation = triangulation;
            triangulation = do_hermeline<affine>(pts);

            canvas_draw_triangulation(old_triangulation, Color::Red, false);
            canvas_draw_triangulation(triangulation, Color::Green, false);
        }

        if (k == Key::O) {
            pts.push_back({round(pt[0]), round(pt[1])});

            canvas_clear();
            old_triangulation = triangulation;
            triangulation = do_hermeline<affine>(pts);

            canvas_draw_triangulation(old_triangulation, Color::Red, false);
            canvas_draw_triangulation(triangulation, Color::Green, false);
        }

        if (k == Key::R) {
            static std::random_device rd;
            static std::mt19937 gen(rd());
//            static std::uniform_real_distribution<float> distrib(-10.f, 10.f);
            static std::uniform_int_distribution<int> distrib(-10, 10);

            pts.clear();
            for (size_t i = 0; i < 50; ++i) {
                pts.emplace_back(distrib(gen), distrib(gen));
            }

            canvas_clear();
            canvas_draw_triangulation(do_hermeline<affine>(pts), Color::Green, false);
        }

        if (k == Key::P) {
            static std::vector<projective::Point> pts_proj;
            pts_proj.clear();
            pts_proj.reserve(pts.size());

            for (const auto& p : pts) {
                pts_proj.push_back({p[0], p[1], 1});
            }

            auto triangu = do_hermeline<projective>(pts_proj);

            canvas_clear();
            canvas_draw_triangulation(do_hermeline<affine>(pts), Color::Red, false);
            auto end = triangu.tri_end();
            for (auto it = triangu.tri_begin(); it != end; ++it) {
                std::array<affine::Point, 3> tri;
                for (size_t i = 0; i < 3; ++i) {
                    const auto& pt = triangu.pt(it->second[i]);
                    tri[i] = {pt[0], pt[1]};
                }
                canvas_draw_triangle(tri, Color::Green, false);
            }
            canvas_draw_point({0, 0}, Color::Blue);
        }
    };

    canvas_show();

    return 0;
}
