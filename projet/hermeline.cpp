#include "hermeline.h"
#include <iostream>
#include <deque>

#include "io.h"

template<typename SpaceT>
void far_points(const std::vector<typename SpaceT::Point>& pts,
                typename SpaceT::Point& box_top_right, typename SpaceT::Point& box_bottom_left,
                typename SpaceT::Point& box_bottom_right, typename SpaceT::Point& box_top_left);

template<> void far_points<affine>(const std::vector<typename affine::Point>& pts,
                                   typename affine::Point& box_top_right, typename affine::Point& box_bottom_left,
                                   typename affine::Point& box_bottom_right, typename affine::Point& box_top_left) {
    typedef affine::Point Point;

    const Point far_point = Point::Constant(std::pow(10.f, 6));
    box_top_right = Point::Constant(std::numeric_limits<float>::lowest());
    box_bottom_left = Point::Constant(std::numeric_limits<float>::max());

    for(const Point& pt : pts)
    {
        box_top_right = box_top_right.cwiseMax(pt);
        box_bottom_left = box_bottom_left.cwiseMin(pt);
    }

    box_top_right += far_point;
    box_bottom_left -= far_point;
    box_bottom_right = {box_top_right.x(), box_bottom_left.y()},
    box_top_left = {box_bottom_left.x(), box_top_right.y()};
}

template<> void far_points<projective>(const std::vector<typename projective::Point>&,
                                       typename projective::Point& box_top_right, typename projective::Point& box_bottom_left,
                                       typename projective::Point& box_bottom_right, typename projective::Point& box_top_left) {
    float far = 10000;
    box_top_right = {far, far, 0};
    box_bottom_left = {-far, -far, 0};
    box_bottom_right = {far, -far, 0};
    box_top_left = {-far, far, 0};
}

template<typename SpaceT>
typename SpaceT::Triangulation do_hermeline_generic(const std::vector<typename SpaceT::Point>& pts) {
    typedef typename SpaceT::Point Point;
    typedef typename SpaceT::Triangulation::TriId TriId;

    Point box_top_right, box_bottom_left, box_bottom_right, box_top_left;
    far_points<SpaceT>(pts, box_top_right, box_bottom_left, box_bottom_right, box_top_left);

    typename SpaceT::Triangulation triangulation;
    auto p1 = triangulation.add_pt(box_bottom_left);
    auto p2 = triangulation.add_pt(box_bottom_right);
    auto p3 = triangulation.add_pt(box_top_left);
    auto p4 = triangulation.add_pt(box_top_right);
    triangulation.add_tri({p1, p3, p2});
    triangulation.add_tri({p4, p3, p2});

    for(const Point& pt : pts)
    {
        // Faire une liste new_tris initialisée avec les nouveaux triangles créés suite à l'ajout du point
        auto new_t = triangulation.tri_add_pt(pt);
        std::vector<TriId> pt_tri_ring1(new_t.begin(), new_t.end());

        // File fifo représentant les triangles restants à traiter
        std::deque<TriId> new_tris;
        for(TriId tri : new_t) new_tris.push_back(tri);

        // Tant qu'il reste des triangles à traiter dans la file
        while(new_tris.size() != 0)
        {
            auto new_tri = new_tris.front();
            if(new_tri == -1)
            {
                new_tris.pop_front();
                continue;
            }

            // 1) Détecter le triangle adjacent [adj_tri] à new_tri qui ne soit pas un triangle de new_tri_list
            auto adj_tris = triangulation.find_adj_tris(new_tri);
            TriId adj_tri = -1;
            for(auto tri : adj_tris)
            {
                if(tri == -1) continue;
                if(std::find(pt_tri_ring1.begin(), pt_tri_ring1.end(), tri) == pt_tri_ring1.end())
                {
                    adj_tri = tri;
                    break;
                }
            }

            // Si on n'a pas trouvé de triangle adjacent qui ne soit pas dans new_tri_list, il n'y a rien à faire
            if(adj_tri == -1)
            {
                new_tris.pop_front();
                continue;
            }

            // 2) Chopper le sommet [opposed_pt] de adj_tri qui n'est pas partagé avec new_tri
            auto opposed_pt = triangulation.find_common_and_diff_pts(new_tri, adj_tri).second[1];

            /* 3) Vérifier si opposed_pt n'est pas dans le cercle circonscrit à new_tri
            *         => Si il est dedans, alors réaliser un flip d'arrête entre new_tri et adj_tri,
            *            ajouter à la file new_tris avec les deux nouveaux triangles pour les traiter
            *            et enlever new_tri de la liste new_tri_list
            */
            if(SpaceT::in_circle(triangulation.tri_pts(new_tri), triangulation.pt(opposed_pt)) == InCircleRes::Inside)
            {
                auto added_tris = triangulation.flip_edge(new_tri, adj_tri);
                new_tris.push_back(added_tris[0]);
                new_tris.push_back(added_tris[1]);

                pt_tri_ring1.erase(std::remove(pt_tri_ring1.begin(), pt_tri_ring1.end(), new_tri), pt_tri_ring1.end());
                pt_tri_ring1.push_back(added_tris[0]);
                pt_tri_ring1.push_back(added_tris[1]);
            }

            // Le triangle courant à été traité, on peut l'enlever de la file
            new_tris.pop_front();
        }
    }

    // 4) Retirer les 4 sommets de la bounding box
    triangulation.remove_pt(p1);
    triangulation.remove_pt(p2);
    triangulation.remove_pt(p3);
    triangulation.remove_pt(p4);

    return triangulation;
}

template<> affine::Triangulation do_hermeline<affine>(const std::vector<affine::Point> &pts)
{
    auto triangulation = do_hermeline_generic<affine>(pts);

    // 5) On colorie les triangles qui sont encore dans des cercles
    auto end_tri = triangulation.tri_end();
    for (auto tri = triangulation.tri_begin(); tri != end_tri; ++tri) {
        auto tri_pts_id = triangulation.tri_pts_id(tri->first);
        auto tri_pts = triangulation.tri_pts(tri->first);
        auto color = random_color();

        auto end_pt = triangulation.points_end();
        for (auto pt = triangulation.points_begin(); pt != end_pt; ++pt) {
            if (std::find(tri_pts_id.begin(), tri_pts_id.end(), pt->first) != tri_pts_id.end()) continue;
            if (affine::in_circle(tri_pts, pt->second) == InCircleRes::Inside) {
                canvas_draw_triangle(tri_pts, color, true);
                canvas_draw_point(pt->second, color);
            }
        }
    }

    return triangulation;
}

template<> projective::Triangulation do_hermeline<projective>(const std::vector<projective::Point> &pts) {
    return do_hermeline_generic<projective>(pts);
}
