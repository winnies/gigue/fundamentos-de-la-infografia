#include "io.h"

#include <fstream>
#include <regex>
#include <random>

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

typedef affine::Point Point;
typedef affine::Vector Vector;
typedef affine::Segment Segment;
typedef affine::Polygon2D Polygon2D;

GeogebraData read_geogebra_file(const std::string& filename) {
  std::ifstream file(filename);
  std::regex balises("<[\\/!]{0,1}\\w[^>]*>"), point_data("(.+) = \\((.+),\\ (.+)\\)"), polygon("Polygone\\((.+)\\)"),
      polygon_vertices("([^,]+)(?:,\\ )?");

  GeogebraData gd;
  std::string line;
  std::smatch match;
  while (std::getline(file, line)) {
    line = std::regex_replace(line, balises, "");
    if (std::regex_search(line, match, point_data)) {
      gd.points[match[1]] = {std::stof(match[2]), std::stof(match[3])};
    } else if (std::regex_search(line, match, polygon)) {
      line              = match[1];
      auto coords_begin = std::sregex_token_iterator(line.begin(), line.end(), polygon_vertices, 1);
      auto coords_end   = std::sregex_token_iterator();
      Polygon2D p;
      p.vertices.reserve(std::distance(coords_begin, coords_end));
      for (auto i = coords_begin; i != coords_end; ++i) {
        p.vertices.push_back(gd.points[i->str()]);
      }
      gd.polygons.push_back(std::move(p));
    }
  }

  return gd;
}

std::vector<std::unique_ptr<sf::Drawable>> canvas_objects;

const Point canvas_top_left_init     = Point::Constant(std::numeric_limits<float>::max()),
              canvas_bottom_right_init = Point::Constant(std::numeric_limits<float>::lowest());

Point canvas_top_left     = canvas_top_left_init,
        canvas_bottom_right = canvas_bottom_right_init;

Color CANVAS_CLEAR_COLOR = Color::Black;
Color CANVAS_COLOR = Color::White;
bool CANVAS_FILL = false;

std::function<void(Point)> CANVAS_MOUSE_CLICKED_CALLBACK = nullptr;
std::function<void(Key,Point)> CANVAS_KEY_PRESSED_CALLBACK = nullptr;

void canvas_draw_points(const Point* pts, size_t count) {
    std::vector<sf::Vertex> vertices;
    vertices.reserve(count);

    for (size_t i = 0; i < count; ++i) {
        Point pos(pts[i][0], -pts[i][1]);
        vertices.emplace_back(sf::Vector2f(pos[0], pos[1]), CANVAS_COLOR);
        canvas_top_left     = canvas_top_left.cwiseMin(pos);
        canvas_bottom_right = canvas_bottom_right.cwiseMax(pos);
    }

    auto shape_ptr = std::make_unique<sf::VertexBuffer>(sf::Points, sf::VertexBuffer::Static);
    shape_ptr->create(vertices.size());
    shape_ptr->update(vertices.data());
    canvas_objects.push_back(std::move(shape_ptr));
}

void canvas_draw_point(const Point& point, const sf::Color& color) {
    CANVAS_COLOR = color;
    canvas_draw_points(&point, 1);
}

void canvas_draw_segments(const Segment* sgts, size_t count) {
    std::vector<sf::Vertex> vertices;
    vertices.reserve(count * 2);

    for (size_t i = 0; i < count; ++i) {
        for (const Point* pt : {&sgts[i].p1, &sgts[i].p2}) {
          Point pos((*pt)[0], -(*pt)[1]);
          vertices.emplace_back(sf::Vector2f(pos[0], pos[1]), CANVAS_COLOR);
          canvas_top_left     = canvas_top_left.cwiseMin(pos);
          canvas_bottom_right = canvas_bottom_right.cwiseMax(pos);
        }
    }

    auto shape_ptr = std::make_unique<sf::VertexBuffer>(sf::Lines, sf::VertexBuffer::Static);
    shape_ptr->create(vertices.size());
    shape_ptr->update(vertices.data());
    canvas_objects.push_back(std::move(shape_ptr));
}

void canvas_draw_segment(const Segment& segment, const sf::Color& color) {
    CANVAS_COLOR = color;
    canvas_draw_segments(&segment, 1);
}

void canvas_draw_polygon(const Point *pts, size_t count) {
    size_t v_count = count + (CANVAS_FILL ? 0 : 1);
    std::vector<sf::Vertex> vertices;
    vertices.reserve(v_count);

    for (size_t i = 0; i < v_count; ++i) {
      Point pos(pts[i%count][0], -pts[i%count][1]);
      vertices.emplace_back(sf::Vector2f(pos[0], pos[1]), CANVAS_COLOR);
      canvas_top_left     = canvas_top_left.cwiseMin(pos);
      canvas_bottom_right = canvas_bottom_right.cwiseMax(pos);
    }

    auto shape_ptr = std::make_unique<sf::VertexBuffer>(CANVAS_FILL ? sf::TriangleFan : sf::LineStrip, sf::VertexBuffer::Static);
    shape_ptr->create(vertices.size());
    shape_ptr->update(vertices.data());
    canvas_objects.push_back(std::move(shape_ptr));
}

void canvas_draw_polygon(const Point *pts, size_t count, const sf::Color &color, bool fill) {
    CANVAS_COLOR = color;
    CANVAS_FILL = fill;
    canvas_draw_polygon(pts, count);
}

void canvas_draw_polygon(const Polygon2D& polygon, const sf::Color& color, bool fill) {
    CANVAS_COLOR = color;
    CANVAS_FILL = fill;
    canvas_draw_polygon(polygon.vertices.data(), polygon.vertices.size(), color, fill);
}

void canvas_draw_triangle(const std::array<Point, 3> &tri, const sf::Color &color, bool fill) {
    CANVAS_COLOR = color;
    CANVAS_FILL = fill;
    canvas_draw_polygon(tri.data(), tri.size(), color, fill);
}

void canvas_draw_triangulation(const affine::Triangulation &tri, const sf::Color &color, bool fill)
{
    CANVAS_COLOR = color;
    CANVAS_FILL = fill;

    auto pt_end = tri.points_end();
    for (auto it = tri.points_begin(); it != pt_end; ++it) {
        Point pos(it->second[0], -it->second[1]);
        canvas_top_left     = canvas_top_left.cwiseMin(pos);
        canvas_bottom_right = canvas_bottom_right.cwiseMax(pos);
    }

    if (CANVAS_FILL) {
        std::vector<sf::Vertex> vertices;
        vertices.reserve(tri.tri_count() * 3);

        auto end = tri.tri_end();
        for (auto it = tri.tri_begin(); it != end; ++it) {
            for (auto pt_id : tri.tri_pts_id(it->first)) {
                auto pt = tri.pt(pt_id);
                vertices.emplace_back(sf::Vector2f(pt[0], -pt[1]), CANVAS_COLOR);
            }
        }

        auto shape_ptr = std::make_unique<sf::VertexBuffer>(sf::Triangles, sf::VertexBuffer::Static);
        shape_ptr->create(vertices.size());
        shape_ptr->update(vertices.data());
        canvas_objects.push_back(std::move(shape_ptr));
    } else {
        std::vector<sf::Vertex> vertices;
        vertices.reserve(tri.tri_count() * 6);

        auto tri_end = tri.tri_end();
        for (auto it = tri.tri_begin(); it != tri_end; ++it) {
            auto pts = tri.tri_pts(it->first);
            for (size_t e = 0; e < 3; ++e) {
                for (size_t v = 0; v < 2; ++v) {
                    size_t i = (e + v) % 3;
                    vertices.emplace_back(sf::Vector2f(pts[i][0], -pts[i][1]), CANVAS_COLOR);
                }
            }
        }

        auto shape_ptr = std::make_unique<sf::VertexBuffer>(sf::Lines, sf::VertexBuffer::Static);
        shape_ptr->create(vertices.size());
        shape_ptr->update(vertices.data());
        canvas_objects.push_back(std::move(shape_ptr));
    }
}

void canvas_clear(sf::Color color) {
  CANVAS_CLEAR_COLOR = color;
  canvas_objects.clear();

  canvas_top_left     = canvas_top_left_init;
  canvas_bottom_right = canvas_bottom_right_init;
}

namespace sf {
template <typename T>
Vector2<T> operator*(const Vector2<T>& left, const Vector2<T>& right) {
    return {left.x * right.x, left.y * right.y};
}
template <typename T>
Vector2<T> operator/(const Vector2<T>& left, const Vector2<T>& right) {
    return {left.x / right.x, left.y / right.y};
}
}

void canvas_show(const std::wstring& title) {
  sf::RenderWindow window(sf::VideoMode(800, 600), title);
  window.setFramerateLimit(30);
  window.setVerticalSyncEnabled(true);
  window.setActive(true);

  auto screen = sf::VideoMode::getFullscreenModes().at(0);
  float screen_width = screen.width, screen_height = screen.height;

  if (canvas_top_left == canvas_top_left_init) {
      canvas_top_left = {-10, -10};
  }
  if (canvas_bottom_right == canvas_bottom_right_init) {
      canvas_bottom_right = {10, 10};
  }

  float canvas_width, canvas_height;
  Point canvas_center;
  Point last_canvas_top_left(0, 0), last_canvas_bottom_right(0, 0);
  auto update_canvas = [&]() {
      Vector canvas_diag = canvas_bottom_right - canvas_top_left;
      canvas_width = canvas_diag[0], canvas_height = canvas_diag[1];
      canvas_center = (canvas_bottom_right + canvas_top_left) * 0.5f;
  };
  update_canvas();

  sf::VertexBuffer grid(sf::PrimitiveType::Lines, sf::VertexBuffer::Static);
{
//  float half_max_canvas_width = canvas_height * screen_width * 0.5f,
//        half_max_canvas_height = canvas_width * screen_height * 0.5f;

//  int top = canvas_center[1] - half_max_canvas_height;
//  int bottom = canvas_center[1] + half_max_canvas_height;
//  int left = canvas_center[0] - half_max_canvas_width;
//  int right = canvas_center[0] + half_max_canvas_width;
  int top = -screen_height/2.f;
  int bottom = screen_height/2.f;
  int left = -screen_width/2.f;
  int right = screen_width/2.f;

  Color grid_color(127, 127, 127);
  std::vector<sf::Vertex> vertices;
  vertices.reserve(((bottom - top) + (right - left) + 2) * 2);
  for (int i = top; i <= bottom; ++i) {
      vertices.emplace_back(sf::Vector2f{float(left), float(i)}, grid_color);
      vertices.emplace_back(sf::Vector2f{float(right), float(i)}, grid_color);
  }
  for (int i = left; i <= right; ++i) {
      vertices.emplace_back(sf::Vector2f{float(i), float(top)}, grid_color);
      vertices.emplace_back(sf::Vector2f{float(i), float(bottom)}, grid_color);
  }

  grid.create(vertices.size());
  grid.update(vertices.data());
}

  bool in_move = false;
  sf::Vector2f last_mouse_pos;

  sf::Vector2f view_center, view_size;
  float view_zoom = 1.1f;

  bool show_grid = true;
  float points_size = 20.f, lines_width = 2.f;

  auto update_view = [&]() {
      sf::View v(view_center, view_size);
      v.zoom(view_zoom);
      window.setView(v);
  };

  auto view_fit_canvas = [&]() {
      float width = window.getSize().x, height = window.getSize().y;

      if (canvas_height <= canvas_width*(height/width)) {
          view_size = {canvas_width, canvas_width*(height/width)};
      } else {
          view_size = {canvas_height*(width/height), canvas_height};
      }
      view_center = {canvas_center[0], canvas_center[1]};
      view_zoom = 1.1f;

      update_view();
  };

  // origine du résultat reste inchangée : haut & gauche fenêtre
  auto viewToCanvasWOrigin = [&](const sf::Vector2i& view_space_vec) {
      return sf::Vector2f(view_space_vec) / sf::Vector2f(window.getSize()) * view_size * view_zoom;
  };

  // origine du résultat déplacée : canvas (0, 0)
  auto viewToCanvasCOrigin = [&](const sf::Vector2i& view_space_vec) {
      return viewToCanvasWOrigin(view_space_vec) + view_center - view_size * view_zoom / 2.f;
  };

  view_fit_canvas();

  while (window.isOpen()) {
    if (canvas_top_left != last_canvas_top_left || canvas_bottom_right != last_canvas_bottom_right) {
        update_canvas();
        last_canvas_top_left = canvas_top_left;
        last_canvas_bottom_right = canvas_bottom_right;
    }
    sf::Event event;
    while (window.pollEvent(event)) {
      switch (event.type) {
      case sf::Event::Closed:
          window.close();
          break;
      case sf::Event::Resized: {
          view_fit_canvas();
        } break;
      case sf::Event::KeyReleased:
          switch(event.key.code) {
          case sf::Keyboard::F:
              view_fit_canvas();
              break;
          case sf::Keyboard::G:
              show_grid = !show_grid;
              break;
          case sf::Keyboard::L:
              lines_width = std::max(lines_width + (event.key.shift ? 1 : -1), 1.f);
              break;
          case sf::Keyboard::P:
              points_size = std::max(points_size + (event.key.shift ? 1 : -1), 1.f);
              break;
          default: break;
          }
          if (CANVAS_KEY_PRESSED_CALLBACK) {
              auto p = viewToCanvasCOrigin(sf::Mouse::getPosition(window));
              CANVAS_KEY_PRESSED_CALLBACK(event.key.code, {p.x, -p.y});
          }
          break;
      case sf::Event::MouseButtonPressed:
          if (!sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) break;
          in_move = true;
          last_mouse_pos = viewToCanvasWOrigin({event.mouseButton.x, event.mouseButton.y});
          break;
      case sf::Event::MouseMoved: {
          if (!in_move) break;
          auto mouse_pos = viewToCanvasWOrigin({event.mouseMove.x, event.mouseMove.y});
          sf::Vector2f move = mouse_pos - last_mouse_pos;
          last_mouse_pos = mouse_pos;

          view_center -= move;
          update_view();
      } break;
      case sf::Event::MouseButtonReleased: {
          in_move = false;
          if (CANVAS_MOUSE_CLICKED_CALLBACK) {
              auto p = viewToCanvasCOrigin({event.mouseButton.x, event.mouseButton.y});
              CANVAS_MOUSE_CLICKED_CALLBACK({p.x, -p.y});
          }
      } break;
      case sf::Event::MouseWheelScrolled:
          view_zoom = std::max(view_zoom - event.mouseWheelScroll.delta * 0.1f, 0.1f);
          update_view();
          break;
      default: break;
      }
    }

    window.clear(CANVAS_CLEAR_COLOR);
    if (show_grid) {
        glLineWidth(1.f);
        window.draw(grid);
    }
    glPointSize(points_size);
    glLineWidth(lines_width);
    for (const auto& shape : canvas_objects) {
      window.draw(*shape);
    }
    window.display();
  }
}

sf::Color random_color()
{
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<uint8_t> distrib(0, 255);
    return {distrib(gen), distrib(gen), distrib(gen)};
}
