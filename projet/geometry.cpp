﻿#include "geometry.h"
#include <algorithm>

affine::HalfPlane::HalfPlane(const Segment &s, const Point &p) {
    p1 = s.p1, p2 = s.p2;
    sign = affine::cross(p1 - p, p2 - p);
    assert(sign != 0);
}

affine::HalfPlane::ContainsRes affine::HalfPlane::contains(const Point &p) const
{
    float side = sign * cross(p1 - p, p2 - p);
    if(side == 0) return ContainsRes::OnBoundary;
    else if (side > 0) return ContainsRes::Inside;
    else return ContainsRes::Outside;
}

float affine::cross(const Vector &v1, const Vector &v2)
{
    // z du produit vectoriel (v1, 0) et (v2, 0)
    return v1[0] * v2[1] - v1[1] * v2[0];
}

ContainsRes affine::contains(const std::array<Point, 3> &tri, const Point &pt) {
    short nb_null_signs = 0;
    auto sin_sign = [&](const Vector& v1, const Vector& v2) {
      auto c = cross(v1, v2);
      if (c == 0) ++nb_null_signs;
      return c == 0 ? 0 : (c > 0 ? 1 : -1);
    };

    auto sum = sin_sign(tri[1] - tri[0], pt - tri[0])
             + sin_sign(tri[2] - tri[1], pt - tri[1])
             + sin_sign(tri[0] - tri[2], pt - tri[2]);

    switch (std::abs(sum)) {
    case 3:
        return ContainsRes::Inside;
    case 2:
        return nb_null_signs == 1 ? ContainsRes::OnEdge : ContainsRes::Outside;
    case 1:
        return nb_null_signs == 2 ? ContainsRes::OnCorner : ContainsRes::Outside;
    default:
        return ContainsRes::Outside;
    }
}

InCircleRes affine::in_circle(const std::array<Point, 3> &tri, const Point &pt)
{
    Eigen::Matrix4f matrix;
    matrix << tri[0][0], tri[0][1], pow(tri[0][0], 2) + pow(tri[0][1], 2), 1,
              tri[1][0], tri[1][1], pow(tri[1][0], 2) + pow(tri[1][1], 2), 1,
              tri[2][0], tri[2][1], pow(tri[2][0], 2) + pow(tri[2][1], 2), 1,
              pt[0], pt[1], pow(pt[0], 2) + pow(pt[1], 2), 1;
    float d = matrix.determinant();
    return d == 0.f ? InCircleRes::OnBoundary : (d > 0.f ? InCircleRes::Inside : InCircleRes::Outside);
}

bool affine::is_ccw(const std::array<Point, 3> &tri)
{
    return cross(tri[1]-tri[0], tri[2]-tri[0]) >= 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

affine::Point to2D(const projective::Point& p)
{
    return {p[0], p[1]};
}

InCircleRes projective::in_circle(const std::array<projective::Point, 3> &tri, const projective::Point &pt)
{
    using Point2D = affine::Point;
    using HalfPlane = affine::HalfPlane;
    using Segment = affine::Segment;

    std::vector<Point2D> pts_inf, pts;
    size_t nb_inf = 0;
    for(const Point& p : tri) {
        if(p[2] == 0) {
            ++nb_inf;
            pts_inf.push_back(to2D(p));
        } else {
            pts.push_back(to2D(p));
        }
    }

    switch(nb_inf) {
    case 0 :
        if(pt[2] == 0) return InCircleRes::Outside;
        return affine::in_circle({to2D(tri[0]), to2D(tri[1]), to2D(tri[2])}, to2D(pt));
    case 1 : {
        Point2D p = to2D(pt), p1 = pts[0], p2 = pts[1], p_inf = pts_inf[0];
        if (pt[2] == 0) p += p1;
        HalfPlane h {affine::Segment{p1, p2}, p1 + p_inf};

        switch (h.contains(p))
        {
        case HalfPlane::ContainsRes::Inside : return InCircleRes::Inside;
        case HalfPlane::ContainsRes::Outside : return InCircleRes::Outside;
        case HalfPlane::ContainsRes::OnBoundary : return InCircleRes::OnBoundary;
        }
    };
    case 2 : {
        Point2D p = to2D(pt), p1_inf = pts_inf[0], p2_inf = pts_inf[1], p3 = pts[0];
        if (pt[2] == 0) p += p3;
        HalfPlane h;
        if (affine::cross(p1_inf, p2_inf) == 0) {
            h = HalfPlane{Segment{p3, p3 + p1_inf}, {0, 0}};
        } else {
            h = HalfPlane{Segment{p3, p3 + (p2_inf.normalized()-p1_inf.normalized())}, p3 + p1_inf};
        }

        switch (h.contains(p))
        {
        case HalfPlane::ContainsRes::Inside : return InCircleRes::Inside;
        case HalfPlane::ContainsRes::Outside : return InCircleRes::Outside;
        case HalfPlane::ContainsRes::OnBoundary : return InCircleRes::OnBoundary;
        }
    };
    case 3 : {

        Point2D p = to2D(pt), p1_inf, p2_inf, p3_inf;

        bool ok = false;
        for (int i = 0; i < 3; ++i) {
            Point2D p1 = pts_inf[i], p2 = pts_inf[(i+1)%3];
            if (affine::cross(p1, p2) == 0) {
                p1_inf = p1;
                p2_inf = p2;
                p3_inf = pts_inf[(i+2)%3];
                ok = true;
                break;
            }
        }
        assert(ok);

        HalfPlane h {Segment{p1_inf, p2_inf}, p3_inf};
        switch (h.contains(p))
        {
        case HalfPlane::ContainsRes::Inside : return InCircleRes::Inside;
        case HalfPlane::ContainsRes::Outside : return InCircleRes::Outside;
        case HalfPlane::ContainsRes::OnBoundary : return InCircleRes::OnBoundary;
        }
    };
        default : assert(false);
    }
}

ContainsRes projective::contains(const std::array<Point, 3>& tri, const Point& pt)
{
    using Point2D = affine::Point;
    using HalfPlane = affine::HalfPlane;
    using Segment = affine::Segment;

    if(pt[2] == 0) return ContainsRes::Outside;

    std::vector<Point2D> pts_inf, pts;
    size_t nb_inf = 0;
    for(const Point& p : tri) {
        if(p[2] == 0) {
            ++nb_inf;
            pts_inf.push_back(to2D(p));
        } else {
            pts.push_back(to2D(p));
        }
    }

    switch(nb_inf) {
    case 0 : return affine::contains({to2D(tri[0]), to2D(tri[1]), to2D(tri[2])}, to2D(pt));
    case 1 : {
        Point2D p = to2D(pt), p1 = pts[0], p2 = pts[1], p_inf = pts_inf[0];
        HalfPlane h1 {Segment{p1, p2}, p1 + p_inf};
        HalfPlane h2 {Segment{p1, p1 + p_inf}, p2};
        HalfPlane h3 {Segment{p2, p2 + p_inf}, p1};

        std::array<short, 3> contains_res = {0, 0, 0};
        for(HalfPlane h : {h1, h2, h3}) {
            contains_res[(int)h.contains(p)] += 1;
        }

        if (contains_res[(int)HalfPlane::ContainsRes::Outside] > 0) {
            return ContainsRes::Outside;
        }
        if (contains_res[(int)HalfPlane::ContainsRes::Inside] == 3) {
            return ContainsRes::Inside;
        }
        if (contains_res[(int)HalfPlane::ContainsRes::OnBoundary] == 2) {
            return ContainsRes::OnCorner;
        }
        return ContainsRes::OnEdge;

    };
    case 2 : {
        Point2D p = to2D(pt), p1_inf = pts_inf[0], p2_inf = pts_inf[1], p3 = pts[0];
        if (affine::cross(p1_inf, p2_inf) == 0) {
            HalfPlane h1 {Segment{p3, p3 + p1_inf}, {0, 0}};
            HalfPlane h2 {Segment{{0, 0}, p1_inf}, p3};
            std::array<short, 3> contains_res = {0, 0, 0};
            for(HalfPlane h : {h1, h2}) {
                contains_res[(int)h.contains(p)] += 1;
            }
            if (contains_res[(int)HalfPlane::ContainsRes::Outside] > 0) {
                return ContainsRes::Outside;
            }
            if (contains_res[(int)HalfPlane::ContainsRes::Inside] == 2) {
                return ContainsRes::Inside;
            }
            return ContainsRes::OnEdge;
        } else {
            HalfPlane h1 {Segment{p3, p3 + p1_inf}, p3 + p2_inf};
            HalfPlane h2 {Segment{p3, p3 + p2_inf}, p3 + p1_inf};
            std::array<short, 3> contains_res = {0, 0, 0};
            for(HalfPlane h : {h1, h2}) {
                contains_res[(int)h.contains(p)] += 1;
            }
            if (contains_res[(int)HalfPlane::ContainsRes::Outside] > 0) {
                return ContainsRes::Outside;
            }
            if (contains_res[(int)HalfPlane::ContainsRes::Inside] == 2) {
                return ContainsRes::Inside;
            }
            if (contains_res[(int)HalfPlane::ContainsRes::OnBoundary] == 2) {
                return ContainsRes::OnCorner;
            }
            return ContainsRes::OnEdge;
        }
    };
    case 3 : {

        Point2D p = to2D(pt), p1_inf, p2_inf, p3_inf;

        bool ok = false;
        for (int i = 0; i < 3; ++i) {
            Point2D p1 = pts_inf[i], p2 = pts_inf[(i+1)%3];
            if (affine::cross(p1, p2) == 0) {
                p1_inf = p1;
                p2_inf = p2;
                p3_inf = pts_inf[(i+2)%3];
                ok = true;
                break;
            }
        }
        assert(ok);

        HalfPlane h {Segment{p1_inf, p2_inf}, p3_inf};
        switch (h.contains(p))
        {
        case HalfPlane::ContainsRes::Inside : return ContainsRes::Inside;
        case HalfPlane::ContainsRes::Outside : return ContainsRes::Outside;
        case HalfPlane::ContainsRes::OnBoundary : return ContainsRes::OnEdge;
        }
    };
        default : assert(false);
    }
}

bool projective::is_ccw(const std::array<projective::Point, 3> &tri)
{
    int i_fini = -1;
    size_t nb_inf = 0;
    for(size_t i = 0; i < 3; i++){
        if(tri[i][2] == 0) {
            nb_inf ++;
        } else {
            i_fini = i;
        }
    }

    switch(nb_inf) {
    case 0 :
    case 3 : return affine::is_ccw({to2D(tri[0]), to2D(tri[1]), to2D(tri[2])});
    case 1 :
    case 2 : {
        assert(i_fini != -1);
        std::array<affine::Point, 3> tri2d;
        for(size_t i = 0; i < 3; i++){
            tri2d[i] = to2D(tri[i]);
            if (tri[i][2] == 0) {
                tri2d[i] += to2D(tri[i_fini]);
            }
        }
        return affine::is_ccw(tri2d);
    };
    default : assert(false);
    }
}

void projective::tests()
{
    assert(projective::contains({projective::Point{0, 0, 1}, {3, 0, 1}, {0, 3, 1}}, {1, 1, 1}) == ContainsRes::Inside);
    assert(projective::contains({projective::Point{0, 0, 1}, {3, 0, 1}, {0, 3, 1}}, {-1, -1, 1}) == ContainsRes::Outside);
    assert(projective::contains({projective::Point{0, 0, 1}, {3, 0, 1}, {0, 3, 1}}, {0.5, 0, 1}) == ContainsRes::OnEdge);
    assert(projective::contains({projective::Point{0, 0, 1}, {3, 0, 1}, {0, 3, 1}}, {0, 3, 1}) == ContainsRes::OnCorner);

    assert(projective::contains({projective::Point{1, -1, 0}, {3, 0, 1}, {0, -3, 1}}, {3, -3, 1}) == ContainsRes::Inside);
    assert(projective::contains({projective::Point{1, -1, 0}, {3, 0, 1}, {0, -3, 1}}, {1000, -1000, 1}) == ContainsRes::Inside);
    assert(projective::contains({projective::Point{1, -1, 0}, {3, 0, 1}, {0, -3, 1}}, {20, -3, 1}) == ContainsRes::Outside);
    assert(projective::contains({projective::Point{1, -1, 0}, {3, 0, 1}, {0, -3, 1}}, {1, -4, 1}) == ContainsRes::OnEdge);
    assert(projective::contains({projective::Point{1, -1, 0}, {3, 0, 1}, {0, -3, 1}}, {3, 0, 1}) == ContainsRes::OnCorner);

    assert(projective::contains({projective::Point{1, -1, 0}, {1, 1, 0}, {3, -3, 1}}, {4, -3, 1}) == ContainsRes::Inside);
    assert(projective::contains({projective::Point{1, -1, 0}, {1, 1, 0}, {3, -3, 1}}, {1000, 0, 1}) == ContainsRes::Inside);
    assert(projective::contains({projective::Point{1, -1, 0}, {1, 1, 0}, {3, -3, 1}}, {6, 0, 1}) == ContainsRes::OnEdge);
    assert(projective::contains({projective::Point{1, -1, 0}, {1, 1, 0}, {3, -3, 1}}, {3, -3, 1}) == ContainsRes::OnCorner);
    assert(projective::contains({projective::Point{1, -1, 0}, {1, 1, 0}, {3, -3, 1}}, {14, 15, 1}) == ContainsRes::Outside);

    assert(projective::contains({projective::Point{1, -1, 0}, {1, 1, 0}, {-1, 1, 0}}, {3, 3, 1}) == ContainsRes::Inside);
    assert(projective::contains({projective::Point{1, -1, 0}, {1, 1, 0}, {-1, 1, 0}}, {10000, 10000, 1}) == ContainsRes::Inside);
    assert(projective::contains({projective::Point{1, -1, 0}, {1, 1, 0}, {-1, 1, 0}}, {4, -4, 1}) == ContainsRes::OnEdge);
    assert(projective::contains({projective::Point{1, -1, 0}, {1, 1, 0}, {-1, 1, 0}}, {-6, 1, 1}) == ContainsRes::Outside);

    assert(projective::in_circle({projective::Point{0, 0, 1}, {3, -3, 1}, {1, 1, 0}}, {100, 55, 1}) == InCircleRes::Inside);
    assert(projective::in_circle({projective::Point{0, 0, 1}, {3, -3, 1}, {1, 1, 0}}, {-16, 15, 1}) == InCircleRes::Outside);
    assert(projective::in_circle({projective::Point{0, 0, 1}, {3, -3, 1}, {1, 1, 0}}, {43, -43, 1}) == InCircleRes::OnBoundary);

    assert(projective::in_circle({projective::Point{1, -1, 0}, {3, -3, 1}, {1, 1, 0}}, {4, -35, 1}) == InCircleRes::Inside);
    assert(projective::in_circle({projective::Point{1, -1, 0}, {3, -3, 1}, {1, 1, 0}}, {3, 52, 1}) == InCircleRes::OnBoundary);
    assert(projective::in_circle({projective::Point{1, -1, 0}, {3, -3, 1}, {1, 1, 0}}, {2, 24, 1}) == InCircleRes::Outside);
}
