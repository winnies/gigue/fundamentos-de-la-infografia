#version 150

uniform vec2 iResolution;
//in vec2 fragCoord;
//out vec4 fragColor;

void main()
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = gl_FragCoord.xy/iResolution.xy;
    //vec2 mouse = iMouse.xy/iResolution.xy;

    // Time varying pixel color
    //vec3 col = 0.5 + 0.5*cos(iTime+uv.xyx+vec3(0,2,4));
    float ratio = iResolution.x / iResolution.y;
    
    float tiles_width = 0.1f;
    float tiles_height = 0.1f * ratio;
    
    vec2 tiles_size = vec2(tiles_width, tiles_height);
    
    float line_width = 1.f;
    vec2 e = line_width/(iResolution.xy*tiles_size*0.5);
    
    vec2 lines = step(1.f-e, abs(mod((uv)/tiles_size, 1.f)*2.-1.));
    vec3 col = vec3(lines.x + lines.y);
   
    // Output to screen
    gl_FragColor = vec4(col,1.0);
}
