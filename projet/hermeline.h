#ifndef HERMELINE_H
#define HERMELINE_H

#include "geometry.h"

template<typename SpaceT>
typename SpaceT::Triangulation do_hermeline(const std::vector<typename SpaceT::Point>& pts);

#endif
