#ifndef IO_H
#define IO_H

#include "geometry.h"
#include "triangulation.h"
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Graphics/Color.hpp>
#include <unordered_map>

using sf::Color;
using Key = sf::Keyboard::Key;

struct GeogebraData {
  std::vector<affine::Polygon2D> polygons;
  std::unordered_map<std::string, affine::Point> points;
};

GeogebraData read_geogebra_file(const std::string& filename);

extern Color CANVAS_CLEAR_COLOR;
extern Color CANVAS_COLOR;
extern bool CANVAS_FILL;

extern std::function<void(affine::Point)> CANVAS_MOUSE_CLICKED_CALLBACK;
extern std::function<void(Key,affine::Point)> CANVAS_KEY_PRESSED_CALLBACK;

void canvas_draw_points(const affine::Point* pts, size_t count);
void canvas_draw_point(const affine::Point& point, const Color& color = CANVAS_COLOR);

void canvas_draw_segments(const affine::Segment* sgts, size_t count);
void canvas_draw_segment(const affine::Segment& segment, const Color& color = CANVAS_COLOR);

void canvas_draw_polygon(const affine::Point *pts, size_t count);
void canvas_draw_polygon(const affine::Polygon2D& polygon, const Color& color = CANVAS_COLOR, bool fill = CANVAS_FILL);
void canvas_draw_triangle(const std::array<affine::Point, 3>& tri, const Color& color = CANVAS_COLOR, bool fill = CANVAS_FILL);

void canvas_draw_triangulation(const affine::Triangulation& tri, const Color& color = CANVAS_COLOR, bool fill = CANVAS_FILL);

void canvas_clear(Color color = CANVAS_CLEAR_COLOR);
void canvas_show(const std::wstring& title = L"Fundamentos de la infografía");

static const char* CANVAS_HELP = u8"clic souris + shift gauche : déplace la vue\n"
                                   "roulette souris : agrandir/rétrécir la vue\n"
                                   "f : centre la vue sur tous les dessins\n"
                                   "g : active/désactive la grille\n"
                                   "L/l : augmente/diminue la grosseur des lignes\n"
                                   "P/p : augmente/diminue la grosseur des points";

Color random_color();

#endif // IO_H
