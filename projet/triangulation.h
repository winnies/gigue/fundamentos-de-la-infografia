#ifndef TRIANGULATION_H
#define TRIANGULATION_H

#include <vector>
#include <map>
#include <algorithm>
#include <cassert>

template<typename SpaceT>
class Triangulation
{
public:
    typedef typename SpaceT::Point Point;
    typedef int PtId; // -1 = invalide
    typedef int TriId; // -1 = invalide

    // Sets
    PtId add_pt(const Point& pt);
    TriId add_tri(const std::array<PtId,3>& pts);

    void remove_pt(PtId pt);

    // Gets
    const Point& pt(PtId pt) const;

    auto points_begin() const {
        return points.cbegin();
    }

    auto points_end() const {
        return points.cend();
    }

    size_t points_count() const {
        return points.size();
    }

    auto tri_begin() const {
        return triangles.cbegin();
    }

    auto tri_end() const {
        return triangles.cend();
    }

    size_t tri_count() const {
        return triangles.size();
    }

    std::array<PtId, 3> tri_pts_id(TriId tri) const;
    std::array<Point, 3> tri_pts(TriId tri) const;

    // retourne les indices des nouveaux triangles
    // si le pt tombe dans un triangle les trois premier triangle sont valides
    // si le pt tombe dans une arête les quatres triangles sont valides
    std::array<TriId, 4> tri_add_pt(const Point& point);

    // tourne l'arête commune aux deux triangles
    std::array<TriId, 2> flip_edge(TriId tri1, TriId tri2);

    // Retourne une paire de deux tableaux de sommets (de taille 2)
    // Le premier tableau contient les 2 sommets partagés
    // Le second contient les 2 sommets non partagés : le premier pour tr1, le deuxième pour tri2
    std::pair<std::array<PtId, 2>, std::array<PtId, 2>> find_common_and_diff_pts(TriId tri1, TriId tri2) const;

    std::array<TriId, 3> find_adj_tris(TriId tri) const;

private:
    PtId current_pt_id = 0;
    TriId current_tri_id = 0;
    std::map<PtId, Point> points;
    std::map<TriId, std::array<PtId, 3>> triangles;
};

template<typename SpaceT>
typename Triangulation<SpaceT>::PtId Triangulation<SpaceT>::add_pt(const Point &pt)
{
    points[current_pt_id] = pt;
    return current_pt_id++;
}

template<typename SpaceT>
void Triangulation<SpaceT>::remove_pt(PtId pt)
{
    for(auto tri = triangles.begin(); tri != triangles.end(); ) {
        if(std::find(tri->second.begin(), tri->second.end(), pt) != tri->second.end()) {
            tri = triangles.erase(tri);
        } else {
            ++tri;
        }
    }

    points.erase(pt);
}

template<typename SpaceT>
typename Triangulation<SpaceT>::TriId Triangulation<SpaceT>::add_tri(const std::array<PtId, 3> &tri)
{
    std::array<PtId, 3> sorted_tri;
    sorted_tri[0] = tri[0];
    if(SpaceT::is_ccw({pt(tri[0]), pt(tri[1]), pt(tri[2])})) {
        sorted_tri[1] = tri[1]; sorted_tri[2] = tri[2];
    } else {
        sorted_tri[1] = tri[2]; sorted_tri[2] = tri[1];
    }
    triangles[current_tri_id] = sorted_tri;
    return current_tri_id++;
}

template<typename SpaceT>
const typename Triangulation<SpaceT>::Point &Triangulation<SpaceT>::pt(PtId pt) const
{
    return points.at(pt);
}

template<typename SpaceT>
std::array<typename Triangulation<SpaceT>::TriId, 3> Triangulation<SpaceT>::find_adj_tris(TriId tri) const
{
    auto tri_pts = tri_pts_id(tri);
    std::array<TriId, 3> adj_tris = {-1, -1, -1};
    int index = 0;
    for(const auto& other_tri : triangles)
    {
        int count = 0;
        for(const auto& pt : other_tri.second)
        {
            if(std::find(tri_pts.begin(), tri_pts.end(), pt) != tri_pts.end())
            {
                count++;
            }
        }
        if(count == 2) adj_tris[index++] = other_tri.first;
    }
    return adj_tris;
}

template<typename SpaceT>
std::array<typename Triangulation<SpaceT>::PtId, 3> Triangulation<SpaceT>::tri_pts_id(TriId tri) const
{
    return triangles.at(tri);
}

template<typename SpaceT>
std::array<typename Triangulation<SpaceT>::Point, 3> Triangulation<SpaceT>::tri_pts(TriId tri) const
{
    std::array<PtId, 3> pts = triangles.at(tri);
    return {points.at(pts[0]), points.at(pts[1]), points.at(pts[2])};
}

template<typename SpaceT>
std::pair<std::array<typename Triangulation<SpaceT>::PtId, 2>, std::array<typename Triangulation<SpaceT>::PtId, 2>> Triangulation<SpaceT>::find_common_and_diff_pts(TriId tri1, TriId tri2) const
{
    std::array<PtId, 2> common_pts = {-1, -1};
    std::array<PtId, 2> diff_pts = {-1, -1};
    std::array<PtId, 3> tri1_pts = tri_pts_id(tri1);
    std::array<PtId, 3> tri2_pts = tri_pts_id(tri2);

    int i1 = 0;
    int i2 = 0;
    bool found = false;
    for(; i1 < 3; i1++)
    {
        for(i2 = 0; i2 < 3; i2++)
        {
            if(tri1_pts[i1] == tri2_pts[i2])
            {
                found = true;
                break;
            }
        }
        if(found) break;
    }

    assert(i1 < 3 && i2 < 3);

    int index = 0;
    for(int i = 0; i < 3; i++)
    {
        PtId pt1 = tri1_pts[(i + i1) % 3]; PtId pt2 = tri2_pts[(3 + i2 - i) % 3];
        if(pt1 == pt2)
        {
            common_pts[index] = pt1;
            index ++;
        }
        else
        {
            diff_pts[0] = pt1;
            diff_pts[1] = pt2;
        }
    }

    assert(common_pts[0] != -1 && common_pts[1] != -1 && diff_pts[0] != -1 && diff_pts[1] != -1);

    return std::make_pair(common_pts, diff_pts);
}

template<typename SpaceT>
std::array<typename Triangulation<SpaceT>::TriId, 4> Triangulation<SpaceT>::tri_add_pt(const Point &point)
{
    std::array<TriId, 4> result;
    result.fill(-1);

    for (const auto& pt : points) {
        if (pt.second == point) return result;
    }

    std::vector<TriId> tri_ids;
    tri_ids.reserve(2);

    for(auto i = triangles.begin(); i != triangles.end(); i++)
    {
        if(SpaceT::contains(tri_pts(i->first), point) != SpaceT::ContainsRes::Outside) tri_ids.push_back(i->first);
    }

    if(tri_ids.size() == 1) // dans un triangle
    {
        PtId new_pt_id = add_pt(point);
        std::array<PtId, 3> tri_pts_ids = tri_pts_id(tri_ids[0]);
        triangles.erase(tri_ids[0]);

        result[0] = add_tri({tri_pts_ids[0], tri_pts_ids[1], new_pt_id});
        result[1] = add_tri({tri_pts_ids[1], tri_pts_ids[2], new_pt_id});
        result[2] = add_tri({tri_pts_ids[0], tri_pts_ids[2], new_pt_id});
    }
    else if(tri_ids.size() == 2) // sur un segment
    {
        PtId new_pt_id = add_pt(point);
        std::pair<std::array<PtId, 2>, std::array<PtId, 2>> common_and_diff_pts = find_common_and_diff_pts(tri_ids[0], tri_ids[1]);
        std::array<PtId, 2>& common_pts = common_and_diff_pts.first;
        std::array<PtId, 2>& diff_pts = common_and_diff_pts.second;
        triangles.erase(tri_ids[0]);
        triangles.erase(tri_ids[1]);

        result[0] = add_tri({common_pts[0], diff_pts[0], new_pt_id});
        result[1] = add_tri({common_pts[0], diff_pts[1], new_pt_id});
        result[2] = add_tri({common_pts[1], diff_pts[0], new_pt_id});
        result[3] = add_tri({common_pts[1], diff_pts[1], new_pt_id});
    }
    else assert(false);

    return result;
}

template<typename SpaceT>
std::array<typename Triangulation<SpaceT>::TriId, 2> Triangulation<SpaceT>::flip_edge(TriId tri1, TriId tri2)
{
    std::pair<std::array<PtId, 2>, std::array<PtId, 2>> common_and_diff_pts = find_common_and_diff_pts(tri1, tri2);
    std::array<PtId, 2>& common_pts = common_and_diff_pts.first;
    std::array<PtId, 2>& diff_pts = common_and_diff_pts.second;

    triangles.erase(tri1);
    triangles.erase(tri2);

    return {add_tri({common_pts[0], diff_pts[0], diff_pts[1]}),
            add_tri({common_pts[1], diff_pts[0], diff_pts[1]})};
}

#endif // TRIANGULATION_H
