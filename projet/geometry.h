#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <Eigen/Dense>
#include <vector>
#include "triangulation.h"

enum class ContainsRes {
    Inside,
    Outside,
    OnEdge,
    OnCorner
};

enum class InCircleRes {
    Inside,
    Outside,
    OnBoundary
};

struct affine {

typedef Eigen::Vector2f Point;
typedef Eigen::Vector2f Vector;
typedef ::InCircleRes InCircleRes;
typedef ::ContainsRes ContainsRes;
typedef ::Triangulation<affine> Triangulation;

struct Segment {
  Point p1, p2;
};

struct HalfPlane {
    enum class ContainsRes {
        Inside,
        Outside,
        OnBoundary
    };

    HalfPlane() = default;
    HalfPlane(const Segment& s, const Point& p);

    ContainsRes contains(const Point& p) const;

    Point p1, p2;
    float sign;
};

struct Polygon2D {
  std::vector<Point> vertices;
};

static float cross(const Vector& v1, const Vector& v2);

static InCircleRes in_circle(const std::array<Point, 3>& tri, const Point& pt);

static ContainsRes contains(const std::array<Point, 3>& tri, const Point& pt);

static bool is_ccw(const std::array<Point, 3>& tri);

};

struct projective {

typedef Eigen::Vector3f Point;
typedef ::InCircleRes InCircleRes;
typedef ::ContainsRes ContainsRes;
typedef ::Triangulation<projective> Triangulation;

static InCircleRes in_circle(const std::array<Point, 3>& tri, const Point& pt);

static ContainsRes contains(const std::array<Point, 3>& tri, const Point& pt);

static bool is_ccw(const std::array<Point, 3>& tri);

static void tests();

};
#endif // GEOMETRY_H
