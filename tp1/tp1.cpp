#include "geometry.h"
#include "io.h"
#include <iostream>

int main() {
  // Point p1{-4, 2}, p2{1, 0};
  // Vector v1{0, -3}, v2{-2, 0};
  // std::cout << intersection({p1, v1}, {p2, v2}) << std::endl;

  // Point a{1, 1}, b{1, -1}, c{0, 0}, d{2, 0}; // true
  // Point a{1, 1}, b{3, 1}, c{0, 0}, d{2, 0}; // false
  // Point a{1, 1}, b{-1, 1}, c{0, 0}, d{2, 0}; // false
  // Point a{1, 1}, b{1, 0}, c{0, 0}, d{2, 0}; // true
  // Point a{-1, 1}, b{-1, -1}, c{0, 0}, d{2, 0}; // false
  // Point a{-2, -2}, b{5, 1}, c{0, 0}, d{2, 0}; // false
  // std::cout << intersect({a, b}, {c, d}) << std::endl;

  //  auto data1 = read_geogebra_file("/tmp/test.html");
  //  // intersection(data.polygons[0], data.polygons[1]);

  //  Point m{0, 0};

  //  if(contains(data1.polygons[0], m))
  //      std::cout << "YEP" << std::endl;
  //  else
  //      std::cout << "NOPE" << std::endl;

  //  if(contains(data1.polygons[1], m))
  //    std::cout << "YEP" << std::endl;
  //  else
  //    std::cout << "NOPE" << std::endl;

  //  if(contains(data1.polygons[2], m))
  //    std::cout << "YEP" << std::endl;
  //  else
  //    std::cout << "NOPE" << std::endl;

  //  if(contains(data1.polygons[3], m))
  //    std::cout << "YEP" << std::endl;
  //  else
  //    std::cout << "NOPE" << std::endl;

  //  if(contains(data1.polygons[4], m))
  //    std::cout << "YEP" << std::endl;
  //  else
  //    std::cout << "NOPE" << std::endl;

  //  if(contains(data1.polygons[5], m))
  //    std::cout << "YEP" << std::endl;
  //  else
  //    std::cout << "NOPE" << std::endl;

  //  /*if(contains(data1.polygons[6], m))
  //    std::cout << "YEP" << std::endl;
  //  else
  //    std::cout << "NOPE" << std::endl;*/

  auto data = read_geogebra_file("/home/loic/Code/fundamentos-de-la-infografia/geogebra/deux_poly.html");

  auto show_op = [&](const std::string& title, Operation oper) {
    for (const auto& p : op(data.polygons[0], data.polygons[1], oper)) {
      canvas_clear();
      canvas_draw(data.polygons[0], Color::Blue);
      canvas_draw(data.polygons[1], Color::Red);
      canvas_draw(p, Color::Yellow);
      canvas_show(title);
    }
  };
  show_op("Intersection", Intersection);
  show_op("Union", Union);
  show_op("Différence symétrique", SymmetricDiff);

  return 0;
}
