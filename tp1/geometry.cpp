#include "geometry.h"
#include <algorithm>

Point2D Line::operator()(float t) const {
  return v * t + p;
}

Segment to_segment(const Line& l) {
  return {l.p, l.p + l.v};
}

Line to_line(const Segment& s) {
  return {s.p1, s.p2 - s.p1};
}

bool intersect(const Segment& s1, const Segment& s2) {
  auto sin_sign = [](const Vector2D& v1, const Vector2D& v2) {
    // z du produit vectoriel (v1, 0) et (v2, 0)
    return v1[0] * v2[1] - v1[1] * v2[0];
  };
  auto same_side = [&](const Segment& s, const Point2D& p1, const Point2D& p2) {
    auto side = s.p2 - s.p1;
    return (sin_sign(p1 - s.p1, side) >= 0.f) == (sin_sign(p2 - s.p1, side) >= 0.f);
  };
  return !(same_side(s1, s2.p1, s2.p2) || same_side(s2, s1.p1, s1.p2));
}

IntersectionParams intersection_params(const Line& l1, const Line& l2) {
  auto ts = (Matrix2f() << l1.v[0], -l2.v[0], l1.v[1], -l2.v[1]).finished().inverse() * (l2.p - l1.p);
  return {ts[0], ts[1]};
}

Point2D intersection(const Line& l1, const Line& l2) {
  return l1(intersection_params(l1, l2).p1);
}

std::vector<Polygon2D> op(const Polygon2D& poly1, const Polygon2D& poly2, Operation op) {
  using InsertedPointI = size_t;
  using MixedPolyI     = size_t;

  struct InsertedPoint {
    Point2D point;
    std::array<float, 2> param;
    std::array<bool, 2> is_incoming;
    std::array<MixedPolyI, 2> mixed_poly;
  };

  struct MixedPoint {
    enum Type { Original, Inserted };

    Type type;
    size_t index; // in Polygon::vertices or inserted_points
  };

  std::vector<InsertedPoint> inserted_points;
  using InsertedPointsBySegment = std::vector<std::vector<InsertedPointI>>;

  std::array<InsertedPointsBySegment, 2> ipbss;
  ipbss[0].resize(poly1.vertices.size());
  ipbss[1].resize(poly2.vertices.size());

  for (size_t i = 0; i < poly1.vertices.size(); ++i) {
    Segment seg1{poly1.vertices[i], poly1.vertices[(i + 1) % poly1.vertices.size()]};

    for (size_t j = 0; j < poly2.vertices.size(); ++j) {
      Segment seg2{poly2.vertices[j], poly2.vertices[(j + 1) % poly2.vertices.size()]};

      if (!intersect(seg1, seg2)) {
        continue;
      }

      auto line1  = to_line(seg1);
      auto params = intersection_params(line1, to_line(seg2));

      InsertedPoint ip;
      ip.point = line1(params.p1);
      ip.param = {params.p1, params.p2};

      auto ip_index = inserted_points.size();
      inserted_points.push_back(ip);
      ipbss[0][i].push_back(ip_index);
      ipbss[1][j].push_back(ip_index);
    }
  }

  std::array<const Polygon2D*, 2> poly{&poly1, &poly2};

  using MixedPoints = std::vector<MixedPoint>;
  std::array<MixedPoints, 2> mixed_polys;

  for (size_t poly_i = 0; poly_i < 2; ++poly_i) {
    const Polygon2D& current_poly = *poly[poly_i];
    const Polygon2D& other_poly   = *poly[(poly_i + 1) % 2];
    auto& ipbs                  = ipbss[poly_i];
    auto& mixed_poly            = mixed_polys[poly_i];

    mixed_poly.reserve(current_poly.vertices.size() + ipbs.size());
    bool outside = !contains(other_poly, current_poly.vertices[0]);

    for (size_t i = 0; i < current_poly.vertices.size(); ++i) {
      std::sort(ipbs[i].begin(), ipbs[i].end(), [&](InsertedPointI ip1, InsertedPointI ip2) {
        return inserted_points[ip1].param[poly_i] < inserted_points[ip2].param[poly_i];
      });

      mixed_poly.push_back({MixedPoint::Original, i});

      for (InsertedPointI ip : ipbs[i]) {
        inserted_points[ip].mixed_poly[poly_i] = mixed_poly.size();
        mixed_poly.push_back({MixedPoint::Inserted, ip});

        inserted_points[ip].is_incoming[poly_i] = outside;
        outside                                 = !outside;
      }
    }
  }

  std::array<std::vector<MixedPolyI>, 2> starting_points;
  switch (op) {
  case Intersection:
  case Union:
    for (MixedPolyI i = 0; i < mixed_polys[0].size(); ++i) {
      if (mixed_polys[0][i].type == MixedPoint::Inserted && inserted_points[mixed_polys[0][i].index].is_incoming[0]) {
        starting_points[0].push_back(i);
      }
    }
    break;
  case SymmetricDiff:
    for (size_t poly_i = 0; poly_i < 2; ++poly_i) {
      for (MixedPolyI i = 0; i < mixed_polys[poly_i].size(); ++i) {
        if (mixed_polys[poly_i][i].type == MixedPoint::Inserted &&
            !inserted_points[mixed_polys[poly_i][i].index].is_incoming[poly_i]) {
          starting_points[poly_i].push_back(i);
        }
      }
    }
    break;
  }

  std::vector<Polygon2D> result;
  for (size_t base_poly_i = 0; base_poly_i < 2; ++base_poly_i) {
    auto& starting_point = starting_points[base_poly_i];
    while (!starting_point.empty()) {
      Polygon2D new_polygon;
      size_t poly_i = base_poly_i;

      enum Direction { Inside, Outside };
      enum IncrementMPi { Add, Sub };
      Direction direction;
      IncrementMPi incr;
      bool change_poly;
      MixedPolyI mp_i;

      auto update_incr = [&]() {
        const auto& mp = mixed_polys[poly_i][mp_i];
        assert(mp.type == MixedPoint::Inserted);
        const auto& ip = inserted_points[mp.index];

        switch (direction) {
        case Inside:
          if (ip.is_incoming[poly_i]) {
            incr = Add;
          } else {
            incr = Sub;
          }
          break;
        case Outside:
          if (ip.is_incoming[poly_i]) {
            incr = Sub;
          } else {
            incr = Add;
          }
          break;
        }
      };

      auto increment_mp_i = [&]() {
        switch (incr) {
        case Add:
          mp_i = (mp_i + 1) % mixed_polys[poly_i].size();
          break;
        case Sub:
          mp_i = mp_i > 0 ? mp_i - 1 : mixed_polys[poly_i].size() - 1;
          break;
        }
      };

      MixedPolyI first_ip = starting_point[0];
      starting_point.erase(starting_point.begin());
      new_polygon.vertices.push_back(inserted_points[mixed_polys[poly_i][first_ip].index].point);

      switch (op) {
      case Intersection:
        mp_i        = first_ip;
        change_poly = true;
        direction   = Inside;
        break;
      case Union:
        mp_i        = first_ip;
        change_poly = true;
        direction   = Outside;
        break;
      case SymmetricDiff:
        mp_i        = first_ip;
        change_poly = true;
        direction   = Inside;
        break;
      }

      update_incr();
      increment_mp_i();
      for (;; increment_mp_i()) {
        const auto& mp = mixed_polys[poly_i][mp_i];

        if (mp.type == MixedPoint::Inserted) {
          const auto& ip = inserted_points[mp.index];
          if (ip.mixed_poly[base_poly_i] == first_ip) {
            break; // the loop
          }

          starting_point.erase(std::remove(starting_point.begin(), starting_point.end(), ip.mixed_poly[base_poly_i]),
                               starting_point.end());

          new_polygon.vertices.push_back(ip.point);

          if (change_poly) {
            poly_i = (poly_i + 1) % 2;
            mp_i   = ip.mixed_poly[poly_i];
          }

          if (op == SymmetricDiff) {
            switch (direction) {
            case Inside:
              direction = Outside;
              break;
            case Outside:
              direction = Inside;
              break;
            }
          }

          update_incr();
        } else if (mp.type == MixedPoint::Original) {
          new_polygon.vertices.push_back(poly[poly_i]->vertices[mp.index]);
        } else {
          assert(false);
        }
      }

      result.push_back(std::move(new_polygon));
    }
  }

  return result;
}

#include <iostream>

enum Vertical { HAUT, BAS };
enum Horizontal { GAUCHE, DROITE };

void position(const Point2D& ptOri, const Point2D& pt, Vertical& vert, Horizontal& hori) {
  hori = ptOri[0] <= pt[0] ? Horizontal::DROITE : Horizontal::GAUCHE;
  vert = ptOri[1] <= pt[1] ? Vertical::HAUT : Vertical::BAS;
}

bool contains(const Polygon2D& poly, const Point2D& pt) {
  Vertical oldVert, vert;
  Horizontal oldHori, hori;
  int count = 0;

  for (int i = 0; i < poly.vertices.size(); ++i) {
    position(pt, poly.vertices[i], oldVert, oldHori);
    position(pt, poly.vertices[(i + 1) % poly.vertices.size()], vert, hori);

    if (pt == poly.vertices[i]) {
      return true;
    }

    if (oldVert == Vertical::HAUT) {
      if (vert == Vertical::BAS) {
        if (oldHori == Horizontal::DROITE || hori == Horizontal::DROITE) {
          float max_X = std::max(poly.vertices[i][0], poly.vertices[(i + 1) % poly.vertices.size()][0]) + 1.0;
          Line MD     = to_line(Segment{pt, Point2D{max_X, 0}});
          Line AB     = to_line(Segment{poly.vertices[i], poly.vertices[(i + 1) % poly.vertices.size()]});

          float numerateur   = -(MD.p[0] * AB.v[1] - AB.p[0] * AB.v[1] - AB.v[0] * MD.p[1] + AB.v[0] * AB.p[1]);
          float denominateur = (MD.v[0] * AB.v[1] - MD.v[1] * AB.v[0]);

          float k = numerateur / denominateur;
          if (k >= 0 && k <= 1)
            count++;
        }
      } else {
        if ((poly.vertices[i][1] == pt[1])) {
          count++;
          count++;
          if (((oldHori == Horizontal::DROITE && hori == Horizontal::GAUCHE) ||
               (oldHori == Horizontal::GAUCHE && hori == Horizontal::DROITE)))
            return true;
        }
      }
    } else {
      if (vert == Vertical::HAUT) {
        if (hori == Horizontal::DROITE || oldHori == Horizontal::DROITE) {
          float max_X = std::max(poly.vertices[i][0], poly.vertices[(i + 1) % poly.vertices.size()][0]) + 1.0;
          Line MD     = to_line(Segment{pt, Point2D{max_X, 0}});
          Line AB     = to_line(Segment{poly.vertices[i], poly.vertices[(i + 1) % poly.vertices.size()]});

          float k = -(MD.p[0] * AB.v[1] - AB.p[0] * AB.v[1] - AB.v[0] * MD.p[1] + AB.v[0] * AB.p[1]) /
                    (MD.v[0] * AB.v[1] - MD.v[1] * AB.v[0]);
          if (k >= 0 && k <= 1)
            count++;
        }
      }
    }
  }

  std::cout << count << std::endl;

  return count % 2 != 0;
}

float clamp(float v, float lo, float hi) {
  assert(!(hi < lo));
  return (v < lo) ? lo : (hi < v) ? hi : v;
}

float clamped_acos(float value) {
  return std::acos(clamp(value, -1.f, 1.f));
}

float angle(const Vector2D& v1, const Vector2D& v2) {
  return clamped_acos(v1.normalized().dot(v2.normalized()));
}

float signed_angle(const Vector2D& v1, const Vector2D& v2) {
  return std::atan2((Matrix2f() << v1[0], v1[1], v2[0], v2[1]).finished().determinant(), v1.dot(v2));
}
