#ifndef IO_H
#define IO_H

#include "geometry.h"
#include <SFML/Graphics/Color.hpp>
#include <unordered_map>

using sf::Color;

struct GeogebraData {
  std::vector<Polygon2D> polygons;
  std::unordered_map<std::string, Point2D> points;
};

GeogebraData read_geogebra_file(const std::string& filename);

void canvas_draw(const Point2D& point, const Color& color);
void canvas_draw(const Segment& segment, const Color& color);
void canvas_draw(const Polygon2D& polygon, const Color& color, bool fill = false);
void canvas_clear();
void canvas_show(const std::string& title = "Fundamentos de la infografía");
Color random_color();

#endif // IO_H
