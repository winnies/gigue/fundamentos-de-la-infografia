#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <Eigen/Dense>
#include <vector>

using Point2D  = Eigen::Vector2f;
using Vector2D = Eigen::Vector2f;
using Eigen::Matrix2f;

struct Segment {
  Point2D p1, p2;
};

struct Line {
  Point2D p;
  Vector2D v;

  Point2D operator()(float t) const;
};

// Line {p, v} => Segment {p, p + v}
Segment to_segment(const Line& l);

// Segment {p1, p2} => Line {p1, p2 - p1}
Line to_line(const Segment& s);

struct Polygon2D {
  std::vector<Point2D> vertices;
};

bool intersect(const Segment& s1, const Segment& s2);

struct IntersectionParams {
  float p1, p2;
};

IntersectionParams intersection_params(const Line& l1, const Line& l2);
Point2D intersection(const Line& l1, const Line& l2);

enum Operation { Intersection, Union, SymmetricDiff };

std::vector<Polygon2D> op(const Polygon2D& p1, const Polygon2D& p2, Operation op);

// TODO: Laurent
bool contains(const Polygon2D& poly, const Point2D& pt);

float clamp(float v, float lo, float hi);

// retourne acos sur une valeur qui est recoupé entre [-1 , 1]
float clamped_acos(float value);

// retourne un angle entre [0 , π]
float angle(const Vector2D& v1, const Vector2D& v2);

// retourne un angle entre [-π , +π]
float signed_angle(const Vector2D& v1, const Vector2D& v2);

#endif // GEOMETRY_H
