#include "io.h"

#include <fstream>
#include <regex>
#include <random>

#include <SFML/Graphics.hpp>

GeogebraData read_geogebra_file(const std::string& filename) {
  std::ifstream file(filename);
  std::regex balises("<[\\/!]{0,1}\\w[^>]*>"), point_data("(.+) = \\((.+),\\ (.+)\\)"), polygon("Polygone\\((.+)\\)"),
      polygon_vertices("([^,]+)(?:,\\ )?");

  GeogebraData gd;
  std::string line;
  std::smatch match;
  while (std::getline(file, line)) {
    line = std::regex_replace(line, balises, "");
    if (std::regex_search(line, match, point_data)) {
      gd.points[match[1]] = {std::stof(match[2]), std::stof(match[3])};
    } else if (std::regex_search(line, match, polygon)) {
      line              = match[1];
      auto coords_begin = std::sregex_token_iterator(line.begin(), line.end(), polygon_vertices, 1);
      auto coords_end   = std::sregex_token_iterator();
      Polygon2D p;
      p.vertices.reserve(std::distance(coords_begin, coords_end));
      for (auto i = coords_begin; i != coords_end; ++i) {
        p.vertices.push_back(gd.points[i->str()]);
      }
      gd.polygons.push_back(std::move(p));
    }
  }

  return gd;
}

std::vector<std::unique_ptr<sf::Drawable>> canvas_objects;
Point2D canvas_top_left     = Point2D::Constant(std::numeric_limits<float>::max()),
      canvas_bottom_right = Point2D::Constant(std::numeric_limits<float>::min());

void canvas_draw(const Point2D& point, const sf::Color& color) {
  auto shape_ptr = std::make_unique<sf::VertexArray>(sf::Points, 1);
  auto& shape    = *shape_ptr;

  Point2D pos           = {point[0], -point[1]};
  shape[0].position   = {pos[0], pos[1]};
  shape[0].color      = color;
  canvas_top_left     = canvas_top_left.cwiseMin(pos);
  canvas_bottom_right = canvas_bottom_right.cwiseMax(pos);

  canvas_objects.push_back(std::move(shape_ptr));
}

void canvas_draw(const Segment& segment, const sf::Color& color) {
  auto shape_ptr = std::make_unique<sf::VertexArray>(sf::Lines, 2);
  auto& shape    = *shape_ptr;

  Point2D pts[] = {segment.p1, segment.p2};
  for (size_t i = 0; i < 2; ++i) {
    pts[i][1]           = -pts[i][1];
    shape[i].position   = {pts[i][0], pts[i][1]};
    shape[i].color      = color;
    canvas_top_left     = canvas_top_left.cwiseMin(pts[i]);
    canvas_bottom_right = canvas_bottom_right.cwiseMax(pts[i]);
  }

  canvas_objects.push_back(std::move(shape_ptr));
}

void canvas_draw(const Polygon2D& polygon, const sf::Color& color, bool fill) {
    if (fill) {
        auto shape_ptr = std::make_unique<sf::ConvexShape>(polygon.vertices.size());
        auto& shape    = *shape_ptr;
        shape.setFillColor(color);

        for (size_t i = 0; i < polygon.vertices.size(); ++i) {
          Point2D pos           = {polygon.vertices[i][0], -polygon.vertices[i][1]};
          shape.setPoint(i, {pos[0], pos[1]});
          canvas_top_left     = canvas_top_left.cwiseMin(pos);
          canvas_bottom_right = canvas_bottom_right.cwiseMax(pos);
        }

        canvas_objects.push_back(std::move(shape_ptr));

    } else {
        auto shape_ptr = std::make_unique<sf::VertexArray>(sf::LineStrip, polygon.vertices.size() + 1);
        auto& shape    = *shape_ptr;

        for (size_t i = 0; i < polygon.vertices.size(); ++i) {
          Point2D pos           = {polygon.vertices[i][0], -polygon.vertices[i][1]};
          shape[i].position   = {pos[0], pos[1]};
          shape[i].color      = color;
          canvas_top_left     = canvas_top_left.cwiseMin(pos);
          canvas_bottom_right = canvas_bottom_right.cwiseMax(pos);
        }
        shape[shape.getVertexCount() - 1] = shape[0];

        canvas_objects.push_back(std::move(shape_ptr));

    }
}

void canvas_clear() {
  canvas_objects.clear();

  canvas_top_left     = Point2D::Constant(std::numeric_limits<float>::max());
  canvas_bottom_right = Point2D::Constant(std::numeric_limits<float>::min());
}

void canvas_show(const std::string& title) {
  sf::RenderWindow window(sf::VideoMode(800, 600), title);
  window.setVerticalSyncEnabled(true);

  sf::View view(sf::FloatRect(canvas_top_left[0],
                              canvas_top_left[1],
                              canvas_bottom_right[0] - canvas_top_left[0],
                              canvas_bottom_right[1] - canvas_top_left[1]));
  window.setView(view);

  while (window.isOpen()) {
    sf::Event event;
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed)
        window.close();
    }

    window.clear();
    for (const auto& shape : canvas_objects) {
      window.draw(*shape);
    }
    window.display();
  }
}

sf::Color random_color()
{
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<uint8_t> distrib(0, 255);
    return {distrib(gen), distrib(gen), distrib(gen)};
}
